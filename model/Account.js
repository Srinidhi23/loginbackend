const mongoose = require('mongoose');
const { Schema } = mongoose;

const accountSchema = new Schema({
    //this is not secure password is not hidden
    //model of an account
    username: String,
    password: String,

    lastAuthetication: Date,
});

mongoose.model('accounts', accountSchema);
