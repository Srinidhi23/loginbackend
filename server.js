const express = require('express'); 
//console.log(express)
const keys = require('./config/keys.js'); //prod or dev

const app = express();

// database
const mongoose = require('mongoose');
mongoose.connect(keys.mongoURI, {useNewUrlParser: true, useUnifiedTopology: true});

//database models
require('./model/Account');

//set up routes
require('./routes/autheticationRoutes')(app);

//const port = 13756;
app.listen(keys.port, () => {
    console.log("Listening on " + keys.port);
});