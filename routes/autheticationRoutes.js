const mongoose = require('mongoose');
const Account = mongoose.model('accounts');

module.exports = app => {
    //routes
    app.get('/account', async (request, response) => {
        //look for info
        const{rUsername, rPassword} = request.query;
        //console.log(username);
        //console.log(password);

        if(rUsername == null || rPassword == null){
            response.send("Invalid credentials");
            return;
        }
        //if acc with username already exists
        var userAccount = await Account.findOne({username: rUsername});
        if(userAccount == null){
            //create new account
            console.log("Creating new account");
            var newAccount = new Account({
                username : rUsername,
                password : rPassword,

                lastAuthetication : Date.now()
            });
            await newAccount.save();
            response.send(newAccount);
            return;
        }else{
            //check correct pw?
            if(rPassword == userAccount.password){ //not encrypted
                userAccount.lastAuthetication = Date.now();
                await userAccount.save();
                console.log("Retrieving account");
                response.send(userAccount);
                return;
            }
        }
        response.send("Invalid credentials");
    });
}

